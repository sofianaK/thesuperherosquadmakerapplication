package gr.mobile.core.providers.textConstants

import androidx.annotation.StringRes
import gr.mobile.core.providers.common.Provider

interface TextConstantsProvider : Provider {
    fun getStringOrDefault(@StringRes stringRes: Int, key: String = ""): String
}