package gr.mobile.core.providers.textConstants

import android.content.Context

class DefaultTextConstantsProvider(private val appContext: Context) : TextConstantsProvider {

    override fun getStringOrDefault(stringRes: Int, key: String): String {
        return appContext.getString(stringRes)
    }
}