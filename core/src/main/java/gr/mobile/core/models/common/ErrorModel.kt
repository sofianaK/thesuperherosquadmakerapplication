package gr.mobile.core.models.common

interface ErrorModel : Model {
    fun isNetworkError(): Boolean

    fun isHttpProtocolError(): Boolean

    fun isUnexpected(): Boolean

    fun getThrowable(): Throwable
}