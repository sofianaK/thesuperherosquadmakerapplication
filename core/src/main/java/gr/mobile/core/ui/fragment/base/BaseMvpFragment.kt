package gr.mobile.core.ui.fragment.base

import gr.mobile.core.mvp.interactor.base.MvpInteractor
import gr.mobile.core.mvp.presenter.base.MvpPresenter
import gr.mobile.core.mvp.view.base.MvpView
import gr.mobile.core.ui.activity.base.BaseMvpActivity

open class BaseMvpFragment<T : MvpPresenter<MvpView, MvpInteractor>> : BaseFragment(), MvpView {

    protected var presenter: T? = null

    override fun isAttached(): Boolean {
        return activity != null && activity?.isFinishing == false && isAdded
    }

    open fun getBaseMvpActivity(): BaseMvpActivity<*>? {
        if (activity == null || activity !is BaseMvpActivity<*>) {
            return null
        }
        return (activity as BaseMvpActivity<*>)
    }

    open fun getBaseMvpParentFragment(): BaseMvpFragment<*>? {
        if (parentFragment == null || parentFragment !is BaseMvpFragment<*>) {
            return null
        }
        return (parentFragment as BaseMvpFragment<*>)
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter?.detach()
    }
}