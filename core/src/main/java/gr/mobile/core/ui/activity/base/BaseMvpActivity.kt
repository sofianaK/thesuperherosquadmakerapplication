package gr.mobile.core.ui.activity.base

import android.annotation.SuppressLint
import android.content.ActivityNotFoundException
import android.content.Intent
import android.net.Uri
import gr.mobile.core.mvp.interactor.base.MvpInteractor
import gr.mobile.core.mvp.presenter.base.MvpPresenter
import gr.mobile.core.mvp.view.base.MvpView

@SuppressLint("Registered")
abstract class BaseMvpActivity<T : MvpPresenter<MvpView, MvpInteractor>> : BaseActivity(), MvpView {

    protected var presenter: T? = null

    override fun isAttached(): Boolean {
        return !isFinishing
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter?.detach()
    }

    fun openAppInGooglePlay() {
        val appPackageName = "gr.mobile.eurobank.corporate"
        try {
            startActivity(
                Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse("market://details?id=$appPackageName")
                )
            )
        } catch (e: ActivityNotFoundException) {
            startActivity(
                Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse("https://play.google.com/store/apps/details?id=$appPackageName")
                )
            )
        }
    }
}