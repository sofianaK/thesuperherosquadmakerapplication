package gr.mobile.core.ui.custom.frameLayout

import android.content.Context
import android.util.AttributeSet
import android.view.MotionEvent
import android.widget.FrameLayout
import timber.log.Timber

open class InterceptTouchFrameLayout @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : FrameLayout(context, attrs, defStyleAttr) {

    private var shouldIntercept = false

    @Synchronized
    fun setShouldIntercept(shouldIntercept: Boolean) {
        this.shouldIntercept = shouldIntercept
    }

    @Synchronized
    fun shouldIntercept(): Boolean {
        return this.shouldIntercept
    }

    override fun onInterceptTouchEvent(ev: MotionEvent): Boolean {
        if (shouldIntercept()) {
            return true
        }

        return try {
            super.onInterceptTouchEvent(ev)
        } catch (e: IllegalArgumentException) {
            Timber.d(e, "InterceptTouchFrameLayout > onInterceptTouchEvent > error")
            false
        }
    }
}