package gr.mobile.core.ui.fragment.base

import androidx.fragment.app.Fragment
import gr.mobile.core.R
import gr.mobile.core.common.delegates.activityResult.ActivityResultDelegate
import gr.mobile.core.common.delegates.fragmentManager.FragmentManagerDelegate
import gr.mobile.core.common.delegates.permissions.PermissionDelegate
import gr.mobile.core.ui.activity.base.BaseActivity
import java.util.*


open class BaseFragment : Fragment() {

    var isCurrentlyVisible: Boolean = false
        private set

    var uniqueInstanceId: String = UUID.randomUUID().toString()
        private set

    val permissionDelegate: PermissionDelegate? by lazy {
        return@lazy getBaseActivity()?.permissionDelegate
    }
    val fragmentManagerDelegate: FragmentManagerDelegate? by lazy {
        return@lazy FragmentManagerDelegate(childFragmentManager, R.id.fragmentContainer)
    }

    open fun getBaseActivity(): BaseActivity? {
        return activity as? BaseActivity
    }

    @Suppress("UNCHECKED_CAST")
    open fun <T> getBaseActivityAs(): T? {
        return activity as? T
    }

    @Suppress("UNCHECKED_CAST")
    open fun <T> getBaseParentFragmentAs(): T? {
        return parentFragment as? T
    }

    open fun getBaseParentFragment(): BaseFragment? {
        return parentFragment as? BaseFragment
    }

    open fun onBackPressed(): Boolean {
        return fragmentManagerDelegate?.onBackPressed() == true
    }

    @Synchronized
    open fun setVisibleToUser(visibleToUser: Boolean) {
        isCurrentlyVisible = visibleToUser
    }

    @Synchronized
    fun isVisibleToUser(): Boolean {
        return isCurrentlyVisible
    }

    fun getActivityResultDelegate(): ActivityResultDelegate? {
        return getBaseActivity()?.activityResultDelegate
    }

    fun isTablet(): Boolean {
        return resources.getBoolean(R.bool.isTablet)
    }
}