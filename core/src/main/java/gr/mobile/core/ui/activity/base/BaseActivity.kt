package gr.mobile.core.ui.activity.base

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import gr.mobile.core.R
import gr.mobile.core.common.delegates.activityResult.ActivityResultDelegate
import gr.mobile.core.common.delegates.fragmentManager.FragmentManagerDelegate
import gr.mobile.core.common.delegates.permissions.PermissionDelegate
import io.github.inflationx.viewpump.ViewPumpContextWrapper
import kotlinx.android.synthetic.main.activity_root.*


@SuppressLint("Registered")
abstract class BaseActivity : AppCompatActivity() {

    val permissionDelegate: PermissionDelegate? by lazy {
        return@lazy PermissionDelegate(this)
    }
    val fragmentManagerDelegate: FragmentManagerDelegate? by lazy {
        return@lazy FragmentManagerDelegate(supportFragmentManager, R.id.fragmentContainer)
    }
    val activityResultDelegate: ActivityResultDelegate? by lazy {
        return@lazy ActivityResultDelegate(this)
    }

    override fun attachBaseContext(newBase: Context) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase))
    }

    override fun setContentView(layoutResID: Int) {
        super.setContentView(R.layout.activity_root)
        val layout = layoutInflater.inflate(layoutResID, rootView, false)
        rootView.addView(layout)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (activityResultDelegate?.onActivityResult(requestCode, resultCode, data) == true) {
            return
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

    override fun onDestroy() {
        super.onDestroy()
        permissionDelegate?.detach()
        fragmentManagerDelegate?.detach()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                ActivityCompat.finishAfterTransition(this)
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        permissionDelegate?.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    fun disableUserInteraction() {
        containerView?.setShouldIntercept(true)
    }

    fun enableUserInteraction() {
        containerView?.setShouldIntercept(false)
    }

    fun isUserInteractionEnabled(): Boolean {
        if (containerView != null) {
            return !containerView.shouldIntercept()
        }
        return false
    }

    override fun onBackPressed() {
        if (fragmentManagerDelegate?.onBackPressed() == false) {
            finish()
        }
    }
}