package gr.mobile.core.mvp.interactor.base

interface MvpInteractor {
    fun detach()
}