package gr.mobile.core.mvp.presenter.base

import gr.mobile.core.mvp.interactor.base.MvpInteractor
import gr.mobile.core.mvp.view.base.MvpView

interface MvpPresenter<out V : MvpView, out I : MvpInteractor> {
    fun detach()
    fun getView(): V?
    fun getInteractor(): I?
    fun isViewAttached(): Boolean
}