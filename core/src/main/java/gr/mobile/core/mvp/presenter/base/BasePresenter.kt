package gr.mobile.core.mvp.presenter.base

import gr.mobile.core.mvp.interactor.base.MvpInteractor
import gr.mobile.core.mvp.view.base.MvpView
import kotlinx.coroutines.*
import java.lang.ref.WeakReference

open class BasePresenter<out V : MvpView, out I : MvpInteractor>(view: V, interactor: I) :
    MvpPresenter<V, I> {

    private var viewRef: WeakReference<V>? = null
    private var interactor: I? = interactor

    protected var uiDispatcher: CoroutineDispatcher = Dispatchers.Main
    protected var bgDispatcher: CoroutineDispatcher = Dispatchers.IO

    protected var job = SupervisorJob()
    protected var uiScope = CoroutineScope(Dispatchers.Main + job)

    init {
        viewRef = WeakReference(view)
    }

    override fun getView(): V? {
        return viewRef?.get()
    }

    override fun getInteractor(): I? {
        return interactor
    }

    override fun isViewAttached(): Boolean {
        return viewRef != null && viewRef!!.get() != null && viewRef!!.get()!!.isAttached()
    }

    override fun detach() {
        viewRef?.clear()
        interactor?.detach()
        interactor = null
        uiScope.coroutineContext.cancelChildren()
    }

    //FIXME: for unit tests ONLY!!
    fun updateDispatchersForTests(
        uiDispatcher: CoroutineDispatcher,
        bgDispatcher: CoroutineDispatcher,
        uiScope: CoroutineScope
    ) {
        this.uiDispatcher = uiDispatcher
        this.bgDispatcher = bgDispatcher
        this.uiScope = uiScope
    }
}