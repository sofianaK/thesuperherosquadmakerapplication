package gr.mobile.core.mvp.view.base

interface MvpView {
    fun isAttached(): Boolean
}