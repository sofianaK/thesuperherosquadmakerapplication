package gr.mobile.core.common.delegates.fragmentManager


import androidx.fragment.app.FragmentManager
import androidx.transition.Transition
import gr.mobile.core.common.CommonConstants
import gr.mobile.core.common.delegates.base.BaseDelegate
import gr.mobile.core.ui.fragment.base.BaseFragment


class FragmentManagerDelegate(
    fragmentManager: FragmentManager,
    private val fragmentContainerId: Int
) :
    BaseDelegate<FragmentManager>(fragmentManager) {

    private fun getCurrentBaseFragment(): BaseFragment? {
        if (!isAttached()) {
            return null
        }
        val fragments = getAllFragments()
        fragments?.findLast {
            it is BaseFragment && it.isAdded && it.isVisible && it.userVisibleHint
        }?.let {
            if (it is BaseFragment) {
                return it
            }
        }
        return null
    }

    fun onBackPressed(): Boolean {
        if (!isAttached()) {
            return false
        }
        val reference = getReference()
        if (reference != null) {
            val currentBaseFragment = getCurrentBaseFragment()
            if (currentBaseFragment != null && currentBaseFragment.onBackPressed()) {
                return true
            }
            if (reference.backStackEntryCount > 1) {
                updateFragmentVisibility(currentBaseFragment, false)
                popBackStack()
                return true
            }
        }
        return false
    }

    fun onBackPressedFragment(): Boolean {
        if (!isAttached()) {
            return false
        }
        val reference = getReference()
        if (reference != null && reference.backStackEntryCount == 1) {
            val currentBaseFragment = getCurrentBaseFragment()
            if (currentBaseFragment != null && currentBaseFragment.onBackPressed()) {
                return true
            }
            updateFragmentVisibility(currentBaseFragment, false)
            popBackStack()
            return true
        }
        return false
    }


    fun <T : BaseFragment> addFragment(
        baseFragment: T,
        backStackTag: String = CommonConstants.BACKSTACK_GLOBAL,
        enterAnim: Int = 0,
        exitAnim: Int = 0,
        enterTransition: Transition? = null,
        exitTransition: Transition? = null,
        reEnterTransition: Transition? = null,
        sharedElementTransition: Transition? = null
    ) {
        if (!isAttached()) {
            return
        }
        checkAndTriggerCurrentFragmentVisibility(false, baseFragment)
        val fragmentTransaction = getReference()?.beginTransaction()

        if (enterTransition != null) {
            baseFragment.enterTransition = enterTransition
        }

        if (exitTransition != null) {
            baseFragment.exitTransition = exitTransition
        }

        if (reEnterTransition != null) {
            baseFragment.reenterTransition = reEnterTransition
        }

        if (sharedElementTransition != null) {
            baseFragment.sharedElementEnterTransition = sharedElementTransition
        }

        if (enterAnim != 0 && exitAnim != 0) {
            fragmentTransaction?.setCustomAnimations(enterAnim, exitAnim, enterAnim, exitAnim)
        }

        fragmentTransaction
            ?.add(fragmentContainerId, baseFragment, backStackTag)
            ?.addToBackStack(backStackTag)
            ?.commitAllowingStateLoss()
        checkAndTriggerCurrentFragmentVisibility(true, baseFragment)
    }

    fun <T : BaseFragment> replaceFragment(
        baseFragment: T,
        backStackTag: String? = CommonConstants.BACKSTACK_GLOBAL,
        enterAnim: Int = 0,
        exitAnim: Int = 0,
        enterTransition: Transition? = null,
        exitTransition: Transition? = null
    ) {
        if (!isAttached()) {
            return
        }
        checkAndTriggerCurrentFragmentVisibility(false, baseFragment)
        val fragmentTransaction = getReference()?.beginTransaction()

        if (enterTransition != null) {
            baseFragment.enterTransition = enterTransition
        }

        if (exitTransition != null) {
            baseFragment.exitTransition = exitTransition
        }

        if (enterAnim != 0 && exitAnim != 0) {
            fragmentTransaction?.setCustomAnimations(enterAnim, 0, 0, exitAnim)
        }

        fragmentTransaction
            ?.replace(fragmentContainerId, baseFragment)
        backStackTag?.apply { fragmentTransaction?.addToBackStack(this) }
        fragmentTransaction?.commitAllowingStateLoss()
    }

    fun <T : BaseFragment> removeFragment(baseFragment: T, backStackTag: String?) {
        if (!isAttached()) {
            return
        }
        checkAndTriggerCurrentFragmentVisibility(false)
        val fragmentTransaction = getReference()?.beginTransaction()
        fragmentTransaction?.remove(baseFragment)
        if (backStackTag != null) {
            fragmentTransaction?.addToBackStack(backStackTag)
        }
        fragmentTransaction?.commitAllowingStateLoss()
    }

    fun <T : BaseFragment> showFragment(baseFragment: T?, vararg fragmentsToHide: T?) {
        if (!isAttached() || baseFragment == null || !baseFragment.isAdded) {
            return
        }
        val fragmentTransaction = getReference()?.beginTransaction()
        fragmentTransaction?.show(baseFragment)
        for (fragmentToHide in fragmentsToHide) {
            if (fragmentToHide != null && fragmentToHide.isAdded) {
                fragmentTransaction?.hide(fragmentToHide)
            }
        }
        fragmentTransaction?.commitAllowingStateLoss()
        updateFragmentVisibility(baseFragment, true)
    }

    fun <T : BaseFragment> showOrAddFragment(
        baseFragment: T,
        backStackTag: String = CommonConstants.BACKSTACK_GLOBAL,
        enterAnim: Int = 0,
        exitAnim: Int = 0,
        vararg fragmentsToHide: T?
    ) {
        if (!isAttached()) {
            return
        }
        if (baseFragment.isAdded) {
            showFragment(baseFragment, *fragmentsToHide)
        } else {
            addFragment(baseFragment, backStackTag, enterAnim, exitAnim)
        }
    }

    fun popBackStack(backStackTag: String, flags: Int) {
        if (!isAttached()) {
            return
        }
        checkAndTriggerCurrentFragmentVisibility(false)
        getReference()?.popBackStack(backStackTag, flags)
        checkAndTriggerCurrentFragmentVisibility(true)
    }

    fun getAllFragments(): List<androidx.fragment.app.Fragment>? {
        if (!isAttached()) {
            return null
        }
        return getReference()?.fragments
    }

    fun popBackStack() {
        if (!isAttached()) {
            return
        }
        checkAndTriggerCurrentFragmentVisibility(false)
        getReference()?.popBackStackImmediate()
        checkAndTriggerCurrentFragmentVisibility(true)
    }

    fun clearBackStack(tag: String) {
        if (!isAttached()) {
            return
        }
        checkAndTriggerCurrentFragmentVisibility(false)
        getReference()?.popBackStackImmediate(tag, FragmentManager.POP_BACK_STACK_INCLUSIVE)
        checkAndTriggerCurrentFragmentVisibility(true)
    }

    private fun checkAndTriggerCurrentFragmentVisibility(
        visible: Boolean,
        fragmentToIgnore: BaseFragment? = null
    ) {
        if (!isAttached()) {
            return
        }
        val currentFragment = fragmentToIgnore ?: getCurrentBaseFragment()
        getAllFragments()?.forEach {
            if ((it is BaseFragment) && currentFragment?.uniqueInstanceId != it.uniqueInstanceId) {
                updateFragmentVisibility(it, false)
            }
        }
        updateFragmentVisibility(currentFragment, visible)
    }

    private fun updateFragmentVisibility(baseFragment: BaseFragment?, visible: Boolean) {
        baseFragment?.let {
            if (it.isVisibleToUser() != visible) {
                it.setVisibleToUser(visible)
            }
        }
    }

    fun getFragmentByTag(tag: String?): androidx.fragment.app.Fragment? {
        if (!isAttached()) {
            return null
        }
        return getReference()?.findFragmentByTag(tag)
    }
}