package gr.mobile.core.common.extensions

import timber.log.Timber
import java.text.SimpleDateFormat
import java.util.*

fun Long?.toFormattedDate(dateFormat: String, locale: Locale = Locale.ENGLISH): String {
    try {
        if (this == null) {
            return ""
        }
        val simpleDateFormat = SimpleDateFormat(dateFormat, locale)
        return simpleDateFormat.format(this)
    } catch (e: Exception) {
        Timber.d(e)
    }
    return ""
}

fun Long.removeTimeInformationFromTimestamp(): Long {
    val calendar = Calendar.getInstance() // locale-specific
    calendar.time = Date(this)
    calendar[Calendar.HOUR_OF_DAY] = 0
    calendar[Calendar.MINUTE] = 0
    calendar[Calendar.SECOND] = 0
    calendar[Calendar.MILLISECOND] = 0
    return calendar.timeInMillis
}
