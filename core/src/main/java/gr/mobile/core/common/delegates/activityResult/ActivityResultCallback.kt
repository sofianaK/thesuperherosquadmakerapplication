package gr.mobile.core.common.delegates.activityResult

import android.content.Intent

interface ActivityResultCallback {
    fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?)

    fun getRequestCode(): Int
}