package gr.mobile.core.common.extensions

import android.content.Context
import android.util.TypedValue

fun Float.dpToPx(context: Context): Int {
    return TypedValue.applyDimension(
        TypedValue.COMPLEX_UNIT_DIP,
        this,
        context.resources.displayMetrics
    ).toInt()
}

fun Float.dpTpSp(context: Context): Int {
    return (this.dpToPx(context) / context.resources.displayMetrics.scaledDensity).toInt()
}
