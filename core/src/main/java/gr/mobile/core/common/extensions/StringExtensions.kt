package gr.mobile.core.common.extensions

import android.content.Context
import android.graphics.drawable.Drawable
import android.os.Build
import android.text.Html
import android.text.Spanned
import android.text.TextUtils
import gr.mobile.core.models.common.Model.Companion.INVALID_STRING
import java.util.regex.Pattern

fun String.toHTML(): Spanned {
    return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
        Html.fromHtml(this, Html.FROM_HTML_MODE_LEGACY)
    } else {
        Html.fromHtml(this)
    }
}

fun String.toHtmlWithImageGetter(context: Context): Spanned {
    val imageGetter = Html.ImageGetter { source ->
        val resourceId: Int =
            context.resources.getIdentifier(source, "drawable", context.packageName)
        val drawable: Drawable = context.resources.getDrawable(resourceId)
        drawable.setBounds(0, 0, drawable.intrinsicWidth, drawable.intrinsicHeight)
        drawable
    }

    return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
        Html.fromHtml(this, Html.FROM_HTML_MODE_LEGACY, imageGetter, null)
    } else {
        Html.fromHtml(this, imageGetter, null)
    }
}

fun String.matchesRegex(regex: String): Boolean {
    val pattern = Pattern.compile(regex)
    val matcher = pattern.matcher(this)
    return matcher.matches()
}

fun String?.getPortfolioFormatted(): String {
    val stringBuilder = StringBuilder()

    if (TextUtils.isEmpty(this)) {
        return stringBuilder.toString()
    }

    val portfolioTrimmed: String = this?.trim { it <= ' ' } ?: ""
    if (portfolioTrimmed.length < 2) {
        return portfolioTrimmed
    }

    stringBuilder.append(portfolioTrimmed.substring(0, 2))
    stringBuilder.append("-")
    stringBuilder.append(portfolioTrimmed.substring(2))
    return stringBuilder.toString()
}

fun String?.formatEurobankAccountNumber(): String {
    var accountNumber = this ?: INVALID_STRING
    if (!TextUtils.isEmpty(accountNumber) && accountNumber.length == 20) {
        accountNumber = java.lang.StringBuilder(accountNumber).insert(4, ".").toString()
        accountNumber = java.lang.StringBuilder(accountNumber).insert(9, ".").toString()
        accountNumber = java.lang.StringBuilder(accountNumber).insert(12, ".").toString()
    }
    return accountNumber
}