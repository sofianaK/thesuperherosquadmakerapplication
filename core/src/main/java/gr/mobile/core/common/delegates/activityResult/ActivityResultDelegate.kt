package gr.mobile.core.common.delegates.activityResult

import android.content.Intent
import gr.mobile.core.common.delegates.base.BaseDelegate
import gr.mobile.core.ui.activity.base.BaseActivity

class ActivityResultDelegate(baseActivity: BaseActivity) :
    BaseDelegate<BaseActivity>(baseActivity) {

    private val activityResultCallbackMap: MutableMap<Int, ActivityResultCallback> = mutableMapOf()

    fun startActivityForResult(intent: Intent, activityResultCallback: ActivityResultCallback) {
        if (!isAttached()) {
            return
        }
        getReference()?.apply {
            activityResultCallbackMap[activityResultCallback.getRequestCode()] =
                activityResultCallback
            startActivityForResult(intent, activityResultCallback.getRequestCode())
        }
    }

    fun registerActivityForResult(activityResultCallback: ActivityResultCallback) {
        if (!isAttached()) {
            return
        }
        activityResultCallbackMap[activityResultCallback.getRequestCode()] = activityResultCallback
    }

    fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?): Boolean {
        if (!isAttached()) {
            return false
        }
        val activityResultCallback = activityResultCallbackMap[requestCode]
        activityResultCallback?.let {
            it.onActivityResult(requestCode, resultCode, data)
            activityResultCallbackMap.remove(requestCode)
            return true
        } ?: run {
            return false
        }
    }
}