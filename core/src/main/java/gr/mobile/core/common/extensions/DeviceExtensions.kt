package gr.mobile.core.common.extensions

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import timber.log.Timber

@SuppressLint("WrongConstant")
fun Activity.closeSoftKeyboard() {
    try {
        val inputManager = getSystemService("input_method") as InputMethodManager
        currentFocus?.windowToken?.apply {
            inputManager.hideSoftInputFromWindow(this, InputMethodManager.HIDE_NOT_ALWAYS)
        }
    } catch (e: Exception) {
        Timber.d(e)
    }
}

fun EditText.openSoftKeyboard() {
    try {
        if (this.context == null) {
            return
        }
        val imm = this.context
            .getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.showSoftInput(this, InputMethodManager.SHOW_IMPLICIT)
    } catch (e: Exception) {
        Timber.d(e)
    }
}
