package gr.mobile.core.common.delegates.application

import android.app.Application
import gr.mobile.core.BuildConfig
import gr.mobile.core.R
import gr.mobile.core.common.delegates.base.BaseDelegate
import io.github.inflationx.calligraphy3.CalligraphyConfig
import io.github.inflationx.calligraphy3.CalligraphyInterceptor
import io.github.inflationx.viewpump.ViewPump
import timber.log.Timber

class ApplicationDelegate(
    application: Application
) : BaseDelegate<Application>(application) {

    fun initTimber() {
        if (BuildConfig.DEBUG)
            Timber.plant(Timber.DebugTree())
    }

    fun initCalligraphy(fontPath: String) {
        ViewPump.init(
            ViewPump.builder()
                .addInterceptor(
                    CalligraphyInterceptor(
                        CalligraphyConfig.Builder()
                            .setDefaultFontPath(fontPath)
                            .setFontAttrId(R.attr.fontPath)
                            .build()
                    )
                )
                .build()
        )
    }
}