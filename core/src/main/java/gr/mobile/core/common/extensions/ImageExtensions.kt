package gr.mobile.core.common.extensions

import android.widget.ImageView
import androidx.appcompat.widget.AppCompatImageView
import com.squareup.picasso.Picasso
import gr.mobile.core.models.image.ImageModel

fun AppCompatImageView.loadImage(
    imageModel: ImageModel?,
    errorImageModel: ImageModel = ImageModel()
) {
    if (imageModel == null) {
        this.setImageResource(errorImageModel.resource)
        return
    }
    if (!imageModel.url.isNullOrEmpty()) {
        Picasso.get().load(imageModel.url).into(this)
        return
    }
    if (imageModel.path != null && imageModel.path.exists()) {
        Picasso.get().load(imageModel.path).into(this)
        return
    }
    this.setImageResource(imageModel.resource)
}

fun ImageView.loadImage(
    imageModel: ImageModel?,
    errorImageModel: ImageModel = ImageModel()
) {
    if (imageModel == null) {
        this.setImageResource(errorImageModel.resource)
        return
    }
    if (!imageModel.url.isNullOrEmpty()) {
        Picasso.get().load(imageModel.url).into(this)
        return
    }
    if (imageModel.path != null && imageModel.path.exists()) {
        Picasso.get().load(imageModel.path).into(this)
        return
    }
    this.setImageResource(imageModel.resource)
}