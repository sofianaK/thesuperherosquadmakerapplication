package gr.mobile.core.common.delegates.permissions

interface PermissionAction {
    fun onPermissionGranted()

    fun onPermissionRejected()

    fun onPermissionNeverAskAgainChecked()

    fun getRequestedPermissions(): Array<String>

    fun getPermissionRequestCode(): Int
}