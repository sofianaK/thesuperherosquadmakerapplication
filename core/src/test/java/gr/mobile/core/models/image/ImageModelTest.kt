package gr.mobile.core.models.image

import gr.mobile.core.R
import org.junit.Assert.assertEquals
import org.junit.Assert.assertFalse
import org.junit.Test
import java.io.File

class ImageModelTest {

    @Test
    fun deepClone() {
        val initial = ImageModel("https://google.com", R.drawable.md_btn_selected, File("storage"))

        val result = initial.deepClone()
        assertEquals(result, initial)
        assertFalse(initial === result)
    }
}