package gr.mobile.core.models.common

import org.junit.Assert.assertTrue
import org.junit.Test

class CommonModelTest {

    @Test
    fun test_Double_Non_Null() {
        val nullDouble: Double? = null
        assertTrue(nullDouble.nonNull() == 0.0)
        assertTrue(nullDouble.nonNull(7.0) == 7.0)

        val notNullDouble: Double? = 5.0
        assertTrue(notNullDouble.nonNull() == 5.0)
        assertTrue(notNullDouble.nonNull(9.0) == 5.0)
    }

    @Test
    fun test_Long_Non_Null() {
        val nullLong: Long? = null
        assertTrue(nullLong.nonNull() == -1L)
        assertTrue(nullLong.nonNull(7L) == 7L)

        val notNullLong: Long? = 5L
        assertTrue(notNullLong.nonNull() == 5L)
        assertTrue(notNullLong.nonNull(9L) == 5L)
    }

    @Test
    fun test_Int_Non_Null() {
        val nullInt: Int? = null
        assertTrue(nullInt.nonNull() == -1)
        assertTrue(nullInt.nonNull(7) == 7)

        val notNullInt: Int? = 5
        assertTrue(notNullInt.nonNull() == 5)
        assertTrue(notNullInt.nonNull(9) == 5)
    }

    @Test
    fun test_String_Non_Null() {
        val nullString: String? = null
        assertTrue(nullString.nonNull() == "")
        assertTrue(nullString.nonNull("ok") == "ok")

        val notNullString: String? = "kk"
        assertTrue(notNullString.nonNull() == "kk")
        assertTrue(notNullString.nonNull("kk1") == "kk")
    }
}