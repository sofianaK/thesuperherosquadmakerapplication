package gr.mobile.core.mvp.presenter.base

import com.nhaarman.mockitokotlin2.whenever
import gr.mobile.core.mvp.interactor.base.BaseInteractor
import gr.mobile.core.mvp.interactor.base.MvpInteractor
import gr.mobile.core.mvp.view.base.MvpView
import org.junit.Assert.assertNotNull
import org.junit.Assert.assertNull
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.MockitoAnnotations

class BasePresenterTest {

    @Mock
    lateinit var view: MvpView

    lateinit var interactor: BaseInteractor

    lateinit var presenter: BasePresenter<MvpView, MvpInteractor>

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)

        interactor = BaseInteractor()

        presenter = BasePresenter(view, interactor)
    }

    @Test
    fun detach() {
        whenever(view.isAttached()).thenReturn(true)

        assertNotNull(presenter.getView())
        assertNotNull(presenter.getInteractor())
        assert(presenter.isViewAttached())

        presenter.detach()

        assertNull(presenter.getView())
        assertNull(presenter.getInteractor())
        assert(!presenter.isViewAttached())
    }

    @Test
    fun getView() {
        assertNotNull(presenter.getView())
    }

    @Test
    fun getInteractor() {
        assertNotNull(presenter.getInteractor())
    }

    @Test
    fun isViewAttached() {
        whenever(view.isAttached()).thenReturn(true)
        assert(presenter.isViewAttached())
        whenever(view.isAttached()).thenReturn(false)
        assert(!presenter.isViewAttached())
        whenever(view.isAttached()).thenReturn(true)
        presenter.detach()
        assert(!presenter.isViewAttached())
    }

}