package com.annakoukio.thesuperherosquadmakerapplication.ui.adapter.heroes

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.extensions.LayoutContainer

class HeroesRecyclerViewAdapter :
        RecyclerView.Adapter<HeroesRecyclerViewAdapter.HeroesViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HeroesViewHolder {
        TODO("Not yet implemented")
    }

    override fun getItemCount(): Int {
        TODO("Not yet implemented")
    }

    override fun onBindViewHolder(holder: HeroesViewHolder, position: Int) {
        TODO("Not yet implemented")
    }


    class HeroesViewHolder(override val containerView: View) :
            RecyclerView.ViewHolder(containerView),
            LayoutContainer {}
}