package com.annakoukio.thesuperherosquadmakerapplication.ui.activity.heroes

import android.os.Bundle
import com.annakoukio.thesuperherosquadmakerapplication.R
import com.annakoukio.thesuperherosquadmakerapplication.mvp.presenter.heroes.HeroesListPresenter
import com.annakoukio.thesuperherosquadmakerapplication.mvp.view.heroes.HeroesListView
import gr.mobile.core.ui.activity.base.BaseMvpActivity

class HeroesListActivity : BaseMvpActivity<HeroesListPresenter>(), HeroesListView {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_heroes_list)
    }
}