package com.annakoukio.thesuperherosquadmakerapplication.ui.activity.heroes.details

import android.os.Bundle
import com.annakoukio.thesuperherosquadmakerapplication.R
import com.annakoukio.thesuperherosquadmakerapplication.mvp.presenter.heroes.details.HeroesListDetailsPresenter
import com.annakoukio.thesuperherosquadmakerapplication.mvp.view.heroes.details.HeroesListDetailsView
import gr.mobile.core.ui.activity.base.BaseMvpActivity

class HeroesListDetailsActivity : BaseMvpActivity<HeroesListDetailsPresenter>(),
    HeroesListDetailsView {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_heroes_list_details)
    }
}