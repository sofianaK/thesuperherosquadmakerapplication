package com.annakoukio.thesuperherosquadmakerapplication.application

import android.app.Application
import com.annakoukio.thesuperherosquadmakerapplication.BuildConfig
import timber.log.Timber

class HeroesSquadMakerApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        initTimberLogging()
    }

    private fun initTimberLogging() {
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
    }
}