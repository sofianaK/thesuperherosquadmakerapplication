package com.annakoukio.thesuperherosquadmakerapplication.mvp.presenter.heroes.details

import com.annakoukio.thesuperherosquadmakerapplication.mvp.interactor.heroes.details.HeroesListDetailsInteractor
import com.annakoukio.thesuperherosquadmakerapplication.mvp.view.heroes.details.HeroesListDetailsView
import gr.mobile.core.mvp.presenter.base.MvpPresenter

interface HeroesListDetailsPresenter :
    MvpPresenter<HeroesListDetailsView, HeroesListDetailsInteractor> {
}