package com.annakoukio.thesuperherosquadmakerapplication.mvp.presenter.heroes

import com.annakoukio.thesuperherosquadmakerapplication.mvp.interactor.heroes.HeroesListInteractor
import com.annakoukio.thesuperherosquadmakerapplication.mvp.view.heroes.HeroesListView
import gr.mobile.core.mvp.presenter.base.MvpPresenter

interface HeroesListPresenter : MvpPresenter<HeroesListView, HeroesListInteractor> {
}