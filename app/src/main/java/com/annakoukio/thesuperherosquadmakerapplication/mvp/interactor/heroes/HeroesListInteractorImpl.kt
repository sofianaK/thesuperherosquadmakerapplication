package com.annakoukio.thesuperherosquadmakerapplication.mvp.interactor.heroes

import com.annakoukio.thesuperherosquadmakerapplication.mvp.interactor.heroes.HeroesListInteractor
import gr.mobile.core.mvp.interactor.base.BaseInteractor

class HeroesListInteractorImpl : BaseInteractor(), HeroesListInteractor