package com.annakoukio.thesuperherosquadmakerapplication.mvp.presenter.heroes

import com.annakoukio.thesuperherosquadmakerapplication.mvp.interactor.heroes.HeroesListInteractor
import com.annakoukio.thesuperherosquadmakerapplication.mvp.view.heroes.HeroesListView
import gr.mobile.core.mvp.presenter.base.BasePresenter

class HeroesListPresenterImpl(
        view: HeroesListView,
        interactor: HeroesListInteractor
) : BasePresenter<HeroesListView, HeroesListInteractor>(view,interactor), HeroesListPresenter {

}